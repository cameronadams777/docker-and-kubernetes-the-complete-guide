const express = require('express')
const redis = require('redis')
const process = require('process')

const app = express()
const redisClient = redis.createClient({
  host: 'redis-server',
  port: 6379
})
redisClient.set('visists', 0)

app.get('/', (req, res) => {
  process.exit(0)
  redisClient.get('visits', (err, visits) => {
    redisClient.set('visits', +visits + 1)
    res.send('Number of visits is ' + (visits || 0))
  })
})

app.listen(8080, () => {
  console.log('Listening on port 8080...')
})